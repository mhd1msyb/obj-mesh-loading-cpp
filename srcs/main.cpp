#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/ObjLoader.h"

#include <iostream>

using namespace ci;
using namespace ci::app;
using namespace gl;


class Test: public App {
public:
  void setup() override;
  void draw() override;

  BatchRef cube;
  TextureRef tex;
  CameraPersp camera;

};

void app_settings(Test::Settings* settings){
}

void Test::setup(){
  camera.setPerspective(50.0, getWindowAspectRatio(),0.05,500.0);
  camera.lookAt(vec3(3.0,0.0,0.0), vec3(0.0,0.0,0.0));

  ObjLoader load_mesh(loadFile("cube.obj"));
  cube=Batch::create(load_mesh, getStockShader(ShaderDef().texture()));
  tex=Texture::create(loadImage("grid.png"));
  gl::enableDepthRead(true);
  gl::enableFaceCulling(true);

}

void Test::draw(){
  clear(ci::Color::black(), true);
  setMatrices(camera);
  {
    (*cube).draw();
    (*tex).bind();
  }
}



CINDER_APP(Test, RendererGl, app_settings)
